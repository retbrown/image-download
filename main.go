package main

import (
	"database/sql"
	"log"
	"os"
	"strconv"
	"sync"

	"bitbucket.org/retbrown/image-download/features/download"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
)

func main() {
	wg := new(sync.WaitGroup)
	jobs := make(chan string, 10000)

	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	//read rows without image
	//start workers to download image up to maximum number from parameter
	//when finished download start next
	if len(os.Args[1:]) != 1 {
		log.Fatal("Incorrect number of arguments")
	}

	// define number of workers and setup worker code
	numberOfBuilders, err := strconv.ParseInt(os.Args[1], 10, 0)
	if err != nil {
		log.Fatal("Argument is not a number")
	}

	connStr := os.Getenv("CONN_STRING")
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}

	var w int64
	for w = 1; w <= numberOfBuilders; w++ {
		wg.Add(1)
		go download.DownloadImage(w, db, jobs, wg)
	}

	rows, err := db.Query("SELECT url FROM images WHERE image is null")
	if err != nil {
		log.Fatal(err)
	}

	defer rows.Close()
	for rows.Next() {
		var url string
		err = rows.Scan(&url)
		jobs <- url
	}

	close(jobs)

	wg.Wait()
	os.Exit(0)
}
