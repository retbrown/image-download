package download

import (
	"database/sql"
	b64 "encoding/base64"
	"io/ioutil"
	"log"
	"net/http"
	"sync"
	"time"
)

// Image Structure to define image details
type Image struct {
	url  string
	data []byte
}

// DownloadImage Download function to download url from channel and store
func DownloadImage(id int64, db *sql.DB, jobs <-chan string, wg *sync.WaitGroup) {
	defer wg.Done()

	for url := range jobs {
		start := time.Now()
		log.Printf("Download started: url: %s", url)

		// don't worry about errors
		response, err := http.Get(url)
		if err != nil {
			log.Fatal(err)
		}
		defer response.Body.Close()

		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			log.Fatal(err)
		}

		sEnc := b64.StdEncoding.EncodeToString(body)

		sqlStatement := "UPDATE images set image = $1 WHERE url = $2"
		_, err = db.Exec(sqlStatement, sEnc, url)
		if err != nil {
			log.Fatal(err)
		}

		elapsed := time.Since(start)
		log.Printf("Download completed: url: %s, time: %s", url, elapsed)
	}
}
